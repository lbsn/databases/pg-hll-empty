-- Drop default Schema public
-- for security: https://dba.stackexchange.com/a/98947/139107
DROP SCHEMA IF EXISTS public CASCADE;