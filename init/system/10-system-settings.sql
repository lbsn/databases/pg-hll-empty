/* Adjust system settings for better performance
 * according to http://pgtune.leopard.in.ua/
 * DB Version: 11
 * OS Type: linux
 * DB Type: oltp
 * Total Memory (RAM): 20 GB
 * CPUs num: 4
 * Connections num: 20
 * Data Storage: ssd
 */
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '5GB';
ALTER SYSTEM SET effective_cache_size = '15GB';
ALTER SYSTEM SET maintenance_work_mem = '1280MB';
ALTER SYSTEM SET checkpoint_completion_target = '0.9';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '100';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '128MB';
ALTER SYSTEM SET min_wal_size = '2GB';
ALTER SYSTEM SET max_wal_size = '4GB';
ALTER SYSTEM SET max_worker_processes = '4';
ALTER SYSTEM SET max_parallel_workers_per_gather = '2';
ALTER SYSTEM SET max_parallel_workers = '4';
