-- hlluser
DROP USER IF EXISTS hlluser;

CREATE USER hlluser WITH
    LOGIN
    INHERIT
    PASSWORD :'ROUSERPASSWORD';

GRANT readaccess TO hlluser;

GRANT CONNECT ON DATABASE :DBNAME TO hlluser;
GRANT USAGE ON SCHEMA extensions TO hlluser;
GRANT CREATE ON SCHEMA extensions TO hlluser;
GRANT EXECUTE ON ALL functions IN SCHEMA extensions TO hlluser;
GRANT ALL PRIVILEGES ON TYPE hll TO hlluser;