DROP SCHEMA IF EXISTS extensions CASCADE;

CREATE SCHEMA extensions;


/* Create Hyperloglog extension
 * PostgreSQL extension adding HyperLogLog data structures
 * as a native data type
 * https://github.com/citusdata/postgresql-hll
 */
CREATE EXTENSION IF NOT EXISTS hll SCHEMA extensions;


/* Create PostGIS extension
 * PostGIS is a spatial database extender for PostgreSQL
 * object-relational database. It adds support for geographic
 * objects allowing location queries to be run in SQL.
 * https://postgis.net/
 */
CREATE EXTENSION IF NOT EXISTS postgis SCHEMA extensions;


/* Create Crypto extension
 * for Anonymization on export,
 * e.g. (user_guid = digest('salt' || 'pw','sha256')::varchar) AS "user_hash"
 * https://www.postgresql.org/docs/current/pgcrypto.html
 */
CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA extensions;


/* Create unaccent extension
 * remove accents from lexemes,
 * e.g. SELECT * FROM posts WHERE unaccent(tag) = unaccent('João');
 * https://www.postgresql.org/docs/current/unaccent.html
 */
CREATE EXTENSION IF NOT EXISTS unaccent SCHEMA extensions;


/* Create hstore extension, a specific data type for key-value pairs
 * (e.g. used for optional additional attributes for places);
 */
CREATE EXTENSION IF NOT EXISTS hstore SCHEMA extensions;


/* Search Path for making extensions available in all schemas
Note explicit mentioning of DATABASE name. Further: SQL identifiers that are case-sensitive need to be enclosed in double quotes.
 */
ALTER DATABASE :DBNAME SET search_path TO "$user", extensions;
-- manually
-- SET search_path TO "$user", public, extensions;

/** Grant privileges to users to use
 *  functions and types that will be added in the future
 */
ALTER DEFAULT PRIVILEGES IN SCHEMA extensions GRANT ALL PRIVILEGES ON functions TO postgres;

ALTER DEFAULT PRIVILEGES IN SCHEMA extensions GRANT ALL PRIVILEGES ON types TO postgres;

GRANT USAGE ON SCHEMA extensions TO readaccess;