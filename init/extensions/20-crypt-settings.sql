-- Set global SALT variable for cryptographic hashing
ALTER DATABASE :DBNAME SET crypt.salt = :'CRYPTSALT';