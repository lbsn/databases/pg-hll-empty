[![version](https://lbsn.vgiscience.org/databases/pg-hll-empty/version.svg)](https://gitlab.vgiscience.de/lbsn/databases/pg-hll-empty/tree/master) [![pipeline status](https://gitlab.vgiscience.de/lbsn/databases/pg-hll-empty/badges/master/pipeline.svg)](https://gitlab.vgiscience.de/lbsn/databases/pg-hll-empty/commits/master)

An empty postgres database with the [citus postgres-hll extension](https://github.com/citusdata/postgresql-hll) installed. A `hlluser` (with read-only privileges) and a `SALT`, for server-side cryptographic hashing, are available.

**TL;DR**

Copy `.env.example` and edit default values:

* LOCAL_IP: `127.0.0.1` for local connect, `0.0.0.0` for bind on public IP

Copy `vars.env.example` and edit default values:

* READONLY_USER_PASSWORD: for `hlluser` user with read-only privileges  
* POSTGRES_PASSWORD: for `postgres` user with admin privileges  
* SALT: optionally provide a server-side SALT variable to be used in cryptographic hashing functions, e.g.:

```sql
SELECT hll_add_agg(hll_hash_text(crypt('uniqueid', current_setting('crypt.salt')))) as uniqueid_hll;
```

Default: if READONLY_USER_PASSWORD or SALT are not given, a 32-character string will be generated in `setup.sh`.

Note: For HLL hashing, the `crypt.salt` must stay the same during the entire creation of hll-sets. Make sure to use the same string, e.g. if the server is moved or recreated.

---

To start the docker container use:

```bash
docker-compose up -d
```

To dump and recreate the database container and watch processing the init scripts:

```bash
docker-compose down -v --remove-orphans && docker-compose up --build -d && docker-compose logs --follow
```

---

A complete guide is provided in the [LBSN documentation](https://lbsn.vgiscience.org)