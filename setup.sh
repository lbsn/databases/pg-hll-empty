#!/usr/bin/env bash

[[ -z "$DATABASE_NAME" ]] && $DATABASE_NAME="lbsn"
[[ -z "$READONLY_USER_PASSWORD" ]] && $READONLY_USER_PASSWORD=openssl rand -base64 32
[[ -z "$SALT" ]] && $SALT=openssl rand -base64 32

echo "setting up database: $DATABASE_NAME"

process_file() {

    case "$f" in
        *.sh)
            if [ -x "$f" ]; then
                echo "executing $f"
                "$f"
            else
                echo "sourcing $f"
                source "$f"
            fi
            ;;
        *.sql)
            # if password is passed
            if [ ! -z "$3" ]; then
                echo "running SQL from $f"
                psql -v ON_ERROR_STOP=1 --file "$f" --dbname "$2" --set=ROUSERPASSWORD="$3"
            # if no database name is passed (hint: only system)
            elif [ -z "$2" ]; then
            
                echo "running SQL from $f"
                # exchange default database name with environment variable
                sed -r 's/lbsn/'"$DATABASE_NAME"'/' "$f" | psql -v ON_ERROR_STOP=1
            # if a database name is passed
            else
                echo "running SQL from $f on database $2"
                # run commands from the passed file
                psql -v ON_ERROR_STOP=1 --file "$f" --dbname "$2" --set=CRYPTSALT="$SALT"
            fi
            ;;
        *)      echo "ignoring $f" ;;
    esac
    echo

}

# run everything related to the system and *not* on a certain database
for f in /init/system/*; do
    process_file "$f"
done

# run extension-related commands on the database
for f in /init/extensions/*; do
    process_file "$f" "$DATABASE_NAME"
done

# create users
for f in /init/user/*; do
    process_file "$f" "$DATABASE_NAME" "$READONLY_USER_PASSWORD"
done